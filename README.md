# README #

This project collects streaming tick data from Interactive Brokers. It uses Node.js, ZMQ and Amazon Kinesis. Tick data is collected and passed around using ZMQ then sent to AWS for storage.

### How do I get set up? ###

* Clone the repo.
* $ npm install
* $ pm2 start pm2.json