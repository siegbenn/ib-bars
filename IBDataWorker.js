// Import packages
var zmq = require('zmq')

// Create a socket to publish IB Gateway data.
sock = zmq.socket('pub');
sock.bindSync('tcp://127.0.0.1:3000');

// Set IB Gateway connection parameters.
var ib = new (require('ib'))({
  clientId: 57485,
  host: '127.0.0.1',
  port: 4002

}).on('error', function (err) {
  
  // Log errors to console.
  console.error(Date.now(), 'ERR', err);

}).on('realtimeBar', function (reqId, time, open, high, low, close, volume, wap, count) {

  // Create bar string from values.
  bar = reqId + ',' + time + ',' + open + ',' + high + ',' + low + ',' + close;

  // Publish the bar string to the bar channel.
  sock.send(['bar', bar]);
  
  // console.log(Date.now(), 'SENT_BAR', 'OK');
});

// Connect to the IB Gateway.
ib.connect();

// Request real-time bar data.
ib.reqRealTimeBars(1, ib.contract.forex('EUR','USD'), 5, 'MIDPOINT', false);
ib.reqRealTimeBars(2, ib.contract.forex('USD','JPY'), 5, 'MIDPOINT', false);
ib.reqRealTimeBars(3, ib.contract.forex('GBP','USD'), 5, 'MIDPOINT', false);
ib.reqRealTimeBars(4, ib.contract.forex('USD','CHF'), 5, 'MIDPOINT', false);
ib.reqRealTimeBars(5, ib.contract.forex('AUD','USD'), 5, 'MIDPOINT', false);
ib.reqRealTimeBars(6, ib.contract.forex('USD','CAD'), 5, 'MIDPOINT', false);
//ib.reqRealTimeBars(7, ib.contract.forex('NZD','USD'), 5, 'MIDPOINT', false);