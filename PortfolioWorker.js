require('colors');
var _ = require('lodash');

var ib = new (require('ib'))({
  clientId: 13098403,
  host: '127.0.0.1',
  port: 4002
}).on('error', function (err) {
  console.error(err.message.red);
}).on('position', function (account, contract, pos, avgCost) {
  console.log(
    '%s %s%s %s%s %s%s %s%s',
    '[position]'.cyan,
    'account='.bold, account,
    'contract='.bold, JSON.stringify(contract),
    'pos='.bold, pos,
    'avgCost='.bold, avgCost
  );
})

ib.connect();
ib.reqPositions();
