// Import packages
var AWS = require('aws-sdk');
var zmq = require('zmq');

// Create AWS Firehose connection.
firehose = new AWS.Firehose({apiVersion: '2015-08-04', region: 'us-east-1'});

// Create a socket subscribing to IB Gateway data.
sock = zmq.socket('sub');
sock.connect('tcp://127.0.0.1:3000');
sock.subscribe('bar');


// Initialize array to hold bars.
bars = [];
sock.on('message', function(topic, message) {

  // Convert the bar to a string.
  bar = message.toString('utf8');

  // Add the bar to the bars array.
  bars.push(bar);
  
  // Write to AWS Firehose if we have enough bars.
  if (bars.length >= 100) {
    barsTemp = bars;
    bars = [];
    params = {
      DeliveryStreamName: 'ib-bars',
      Record: {
        Data: barsTemp.join('\n') + '\n'
      }
    };
  
    firehose.putRecord(params, function(err, data) {
      if (err) {
        console.error(Date.now(), 'ERR', err);
      } else {
        console.log(Date.now(), 'SAVED_TO_FIREHOSE', 'OK'); 
      }
    });   
  }
});