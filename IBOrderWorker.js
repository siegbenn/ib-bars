// ===== IMPORTS ===== //
var zmq = require('zmq')
var ib = new (require('ib'))({
  clientId: 9485392,
  host: '127.0.0.1',
  port: 4002
})

// ===== IB MESSAGE FUNCTIONS ===== //
ib.on('error', function (err) {
  console.log('error: ' + Date.now())
  console.error('error: ' + err);
  console.log('===========================================')
})

// ib.on('openOrder', function (orderId, contract, order, orderState) {
//   console.log('openOrder: ' + Date.now())
//   console.log('orderId: ' + orderId)
//   console.log('contract: ' + contract)
//   console.log('order: ' + order)
//   console.log('orderState: ' + orderState)
//   console.log('===========================================')
// })

// ib.on('openOrderEnd', function () {
//   console.log(Date.now() + ': openOrderEnd')
//   console.log('===========================================')
// })

// ib.on('orderStatus', function (id, status, filled, remaining, avgFillPrice, permId, parentId, lastFillPrice, clientId, whyHeld) {
//   console.log('orderStatus: ' + Date.now())
//   console.log('id: ' + id)
//   console.log('status: ' + status)
//   console.log('filled: ' + filled)
//   console.log('remaining: ' + remaining)
//   console.log('avgFillPrice: ' + avgFillPrice)
//   console.log('permId: ' + permId)
//   console.log('parentId: ' + parentId)
//   console.log('lastFillPrice: ' + lastFillPrice)
//   console.log('clientId: ' + clientId)
//   console.log('whyHeld: ' + whyHeld)
//   console.log('===========================================')
// });

// ===== TRADE + ACCOUNT SPECIFIC VARIABLES ===== //
var account = 'DU416836'
var nextOrderId = null
var tradeSize = 50000

var index = {
  1: ib.contract.forex('EUR','USD'), 
  2: ib.contract.forex('USD','JPY'), 
  3: ib.contract.forex('GBP','USD'),  
  4: ib.contract.forex('USD','CHF'), 
  5: ib.contract.forex('AUD','USD'), 
  6: ib.contract.forex('USD','CAD'), 
  7: ib.contract.forex('NZD','USD')
}

var usdQuote = {
  1: true, 
  2: false, 
  3: true,  
  4: false, 
  5: true, 
  6: false, 
  7: true 
}

// ===== ZMQ SIGNAL SETUP ===== //
openSock = zmq.socket('sub')
openSock.connect('tcp://127.0.0.1:3001')
openSock.subscribe('signal')

// ===== ZMQ MESSAGE FUNCTION ===== //
openSock.on('message', function(topic, message) {

  if (nextOrderId == null) {
    ib.reqIds(1)
  } else {

    // Convert the bar to a string.
    signal = message.toString('utf8')
    console.log('open: ' + Date.now())
    console.log(signal)
    console.log('===========================================')


    splitSignal = signal.split(',')

    tradeId = nextOrderId
    nextOrderId += 1

    // If USD is not quote, use default trade size.
    calcTradeSize = tradeSize
    if (usdQuote[splitSignal[1]]) {
        calcTradeSize = (1 / parseFloat(splitSignal[3]) * tradeSize) | 0
    }

    // Create the market order.
    marketOrder = null
    if (splitSignal[0] == 'BUY') {
      marketOrder = ib.order.market('BUY', calcTradeSize)
    } else if (splitSignal[0] == 'SELL') {
      marketOrder = ib.order.market('SELL', calcTradeSize)
    }
    marketOrder.account = account

    // Place the market order.
    ib.placeOrder(tradeId, index[splitSignal[1]], marketOrder)

  }
  
})

// ===== ZMQ SIGNAL SETUP ===== //
closeSock = zmq.socket('sub')
closeSock.connect('tcp://127.0.0.1:3002')
closeSock.subscribe('signal')

// ===== ZMQ MESSAGE FUNCTION ===== //
closeSock.on('message', function(topic, message) {

  if (nextOrderId == null) {
    ib.reqIds(1)
  } else {

    // Convert the bar to a string.
    signal = message.toString('utf8')
    console.log('close: ' + Date.now())
    console.log(signal)
    console.log('===========================================')


    splitSignal = signal.split(',')

    tradeId = nextOrderId
    nextOrderId += 1

    // If USD is not quote, use default trade size.
    calcTradeSize = tradeSize
    if (usdQuote[splitSignal[1]]) {
        calcTradeSize = (1 / parseFloat(splitSignal[3]) * tradeSize) | 0
    }

    // Create the market order.
    marketOrder = null
    if (splitSignal[0] == 'BUY') {
      marketOrder = ib.order.market('BUY', calcTradeSize)
    } else if (splitSignal[0] == 'SELL') {
      marketOrder = ib.order.market('SELL', calcTradeSize)
    }
    marketOrder.account = account

    // Place the market order.
    ib.placeOrder(tradeId, index[splitSignal[1]], marketOrder)

  }
  
})

ib.once('nextValidId', function (orderId) {
  console.log('nextValidId: ' + Date.now())
  console.log('orderId: ' + orderId)
  console.log('===========================================')
  nextOrderId = orderId
});

ib.connect()
ib.reqIds(1);