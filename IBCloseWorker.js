// Import packages
var zmq = require('zmq')

tradeState = {
  1: {
    'BUY': [],
    'SELL': []
  },
  2: {
    'BUY': [],
    'SELL': []
  },
  3: {
    'BUY': [],
    'SELL': []
  },
  4: {
    'BUY': [],
    'SELL': []
  },
  5: {
    'BUY': [],
    'SELL': []
  },
  6: {
    'BUY': [],
    'SELL': []
  },
}

// Create a socket subscribing to IB Gateway data.
dataSock = zmq.socket('sub');
dataSock.connect('tcp://127.0.0.1:3001');
dataSock.subscribe('signal');

// Create a socket to publish signals.
signalSock = zmq.socket('pub');
signalSock.bindSync('tcp://127.0.0.1:3002');

dataSock.on('message', function(topic, message) {
  // Convert the bar to a string.
  signal = message.toString('utf8');

  splitSignal = signal.split(',');


  id = splitSignal[1]
  price = parseFloat(splitSignal[2])
    
  if (splitSignal[0] == "BUY") {
    tradeState[id]["SELL"].push({price: price, base_price: splitSignal[3], side: "SELL", tickerId: id})  
    console.log(JSON.stringify(tradeState))   
  } else if (splitSignal[0] == "SELL") {
    tradeState[id]["BUY"].push({price: price, base_price: splitSignal[3], side: "BUY", tickerId: id})  
    console.log(JSON.stringify(tradeState))   
  }
  
})

function checkTick(tickerId, tickType, price, canAutoExecute) {
  if(canAutoExecute) {
    if(tickType == 1) {
      for (i = 0; i < tradeState[tickerId]["SELL"].length; i++) {
        if (price < tradeState[tickerId]["SELL"][i].price) {
          order = ["SELL", tickerId, null, tradeState[tickerId]["SELL"][i].base_price, null]
          tradeState[tickerId]["SELL"].splice(i, 1)
          signalSock.send(['signal', order.join(",")]);
          console.log("Creating sell order.")
          console.log(JSON.stringify(tradeState))   

          break
        }
      } 
    } else {
      for (i = 0; i < tradeState[tickerId]["BUY"].length; i++) {
        if (price > tradeState[tickerId]["BUY"][i].price) {
          order = ["BUY", tickerId, null, tradeState[tickerId]["BUY"][i].base_price, null]
          tradeState[tickerId]["BUY"].splice(i, 1)
          signalSock.send(['signal', order.join(",")]);
          console.log("Creating buy order.")
          console.log(JSON.stringify(tradeState))   

          break
        }
      } 
    }
  }
}

// Set IB Gateway connection parameters.
var ib = new (require('ib'))({
  clientId: 9785306,
  host: '127.0.0.1',
  port: 4002

}).on('error', function (err) {
  
  // Log errors to console.
  console.error(Date.now(), 'ERR', err);

}).on('tickPrice', function (tickerId, tickType, price, canAutoExecute) {

  // console.log(tickerId, tickType, price, canAutoExecute)
  checkTick(tickerId, tickType, price, canAutoExecute)

 
});

// Connect to the IB Gateway.
ib.connect();

// Request real-time bar data.
ib.reqMktData(1, ib.contract.forex('EUR','USD'), '', false);
ib.reqMktData(2, ib.contract.forex('USD','JPY'), '', false);
ib.reqMktData(3, ib.contract.forex('GBP','USD'), '', false);
ib.reqMktData(4, ib.contract.forex('USD','CHF'), '', false);
ib.reqMktData(5, ib.contract.forex('AUD','USD'), '', false);
ib.reqMktData(6, ib.contract.forex('USD','CAD'), '', false);
//ib.reqMktData(7, ib.contract.forex('NZD','USD'), '', false);

