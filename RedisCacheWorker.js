// Import packages
var redis = require('redis');
var zmq = require('zmq');
var big = require('big-decimal');

// Connect to redis.
client = redis.createClient();

// Create a socket subscribing to IB Gateway data.
dataSock = zmq.socket('sub');
dataSock.connect('tcp://127.0.0.1:3000');
dataSock.subscribe('bar');

// Create a socket to publish signals.
signalSock = zmq.socket('pub');
signalSock.bindSync('tcp://127.0.0.1:3001');

dataSock.on('message', function(topic, message) {
  
  // Convert the bar to a string.
  bar = message.toString('utf8');
  
  // bar = reqId, time, open, high, low, close
  splitBar = bar.split(',');
  
  // Get the index of the current 5-second bar in the day.
  secondInDay = parseInt(splitBar[1]) % 86400 / 5;

  // Create price change args.
  open = new big(splitBar[2]);
  close = new big(splitBar[5]);
  
  priceChange = null
  if (splitBar[0] == 2) {
    priceChange = parseFloat(close.subtract(open).toString()).toFixed(4)

  } else {
      priceChange = parseFloat(close.subtract(open).toString()).toFixed(5)
  }


  args = [splitBar[0], priceChange, secondInDay.toString()];

  redisQueries(splitBar, args, function(err, data) {
    if (err) {
      console.log(err)
    } else {
      if (data != null) {

        // Publish the bar string to the signal channel.
        signalSock.send(['signal', data.join(",")]);
        console.log(data.join(","))

      }
    }
  })

});

var top = 17
var bottom = 17077
var mult = "2.0"

function redisQueries(splitBar, args, callback) {
  client.zadd(args, function(err, res) {
    if (err) {
      
      callback(err, null)
    } else {
      
      client.zcard(args[0][0], function(err, res) {
        if (err) {
          
          callback(err, null)
        } else {
          
          // if (res >= 17094) {
            client.zrevrank(args[0][0], secondInDay, function(err, res) {
              if (err) {

                callback(err, null)
              } else {
                if (res < top) {
                  client.zrevrange(args[0], 10, 10, 'withscores', function(err, zrevrangeRes) {
                    
                    price = new big(zrevrangeRes[1]);
                    stop = new big(splitBar[5]);
                    stopLimitPrice = null

                    if (args[0] == 2) {
                      stopLimitPrice = parseFloat(price.add(stop).toString()).toFixed(4)
                    } else {
                      stopLimitPrice = parseFloat(price.add(stop).toString()).toFixed(5)
                    }
                  
                    callback(null, ["BUY", args[0], stopLimitPrice, splitBar[5], zrevrangeRes[1]])
                  })
                } else if (res > bottom) {
                  client.zrevrange(args[0], top, top, 'withscores', function(err, zrevrangeRes) {
                    
                    price = new big(zrevrangeRes[1]);
                    stop = new big(splitBar[5]);
                    stopLimitPrice = null

                    if (args[0] == 2) {
                      stopLimitPrice = parseFloat(price.add(stop).toString()).toFixed(4)
                    } else {
                      stopLimitPrice = parseFloat(price.add(stop).toString()).toFixed(5)
                    }
                  
                    callback(null, ["SELL", args[0], stopLimitPrice, splitBar[5], zrevrangeRes[1]])
                  })
                } else {
                  callback(null, null)
                } 
              }
            })
          // }
        }
      })      
    }
  })
}